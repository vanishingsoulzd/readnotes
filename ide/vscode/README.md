命令面板：f1 / shift + cmd + p  
终端：cmd + j  
  
查找文件：cmd + p : n  
文件内搜索：cmd + f  
全局搜索：shift + cmd + f  
查找单词：cmd + d  
  
返回：ctrl + -  
  
删除一行：dd  
删除多行：D  
删除：shift + cmd + k  
  
移动代码：opt + 上下  
原地复制：shift + opt + 上下  
  
注释：cmd + /  
块注释：shift + opt + a  
  
跳转：F12 / cmd + F12 / shift + F12  

翻页：ctrl + f / ctrl + b  
翻半页：ctrl + d / ctrl + u  
光标位置：zz / zt / zb  

