# 读书笔记  
## MySQL  
[《高性能MySQL》](https://gitlab.com/vanishingsoulzd/readnotes/-/tree/master/mysql/high_performance_mysql)  
[《MySQL技术内幕：InnoDB存储引擎》](https://gitlab.com/vanishingsoulzd/readnotes/-/tree/master/mysql/innodb_storage_engine)  
[《MySQL实战45讲》](https://gitlab.com/vanishingsoulzd/readnotes/-/tree/master/mysql/mysql_45)  
  
## Linux  
[《简明 VIM 练级攻略》](https://gitlab.com/vanishingsoulzd/readnotes/-/tree/master/linux/vim)  

