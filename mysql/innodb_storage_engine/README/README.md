# 《MySQL技术内幕：InnoDB存储引擎》读书笔记
[第1章　MySQL体系结构和存储引擎](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/innodb_storage_engine/1/%E7%AC%AC1%E7%AB%A0%E3%80%80MySQL%E4%BD%93%E7%B3%BB%E7%BB%93%E6%9E%84%E5%92%8C%E5%AD%98%E5%82%A8%E5%BC%95%E6%93%8E.md)
[第2章　InnoDB存储引擎](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/innodb_storage_engine/2/%E7%AC%AC2%E7%AB%A0%E3%80%80InnoDB%E5%AD%98%E5%82%A8%E5%BC%95%E6%93%8E.md)
[第3章　文件](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/innodb_storage_engine/3/%E7%AC%AC3%E7%AB%A0%E3%80%80%E6%96%87%E4%BB%B6.md)
[第4章　表](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/innodb_storage_engine/4/%E7%AC%AC4%E7%AB%A0%E3%80%80%E8%A1%A8.md)
[第5章　索引与算法](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/innodb_storage_engine/5/%E7%AC%AC5%E7%AB%A0%E3%80%80%E7%B4%A2%E5%BC%95%E4%B8%8E%E7%AE%97%E6%B3%95.md)
[第6章　锁](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/innodb_storage_engine/6/%E7%AC%AC6%E7%AB%A0%E3%80%80%E9%94%81.md)
[第7章　事务](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/innodb_storage_engine/7/%E7%AC%AC7%E7%AB%A0%E3%80%80%E4%BA%8B%E5%8A%A1.md)
