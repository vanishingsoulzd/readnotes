# 《简明 VIM 练级攻略》
出自[《简明 VIM 练级攻略》](https://coolshell.cn/articles/5426.html)  
## 第一级 存活
i  
x  
:wq  
dd 删除当前行  
cc 清除当前行并插入  
p 粘贴剪切板  
hjkl 移动光标  
:help <cmd> 显示帮助  
## 第二级 感觉良好  
a
o/O
w/e/b  
  
**移动光标**  
数字0 到行头  
^ 到第一个不是blank的位置  
$ 行尾  
g_ 到最后一个不是blank的位置  
/pattern 向下搜索 n 下一个  
NG 到第N行  
gg 到第一行  
G 到最后一行  
% 匹配括号移动  
`* #` 匹配单词移动  
fa Fa 到下一个a字符处  
t, T, 到,前一个字符处  
dt" 删除到"的内容  
  
**拷贝/粘贴**  
yy 拷贝当前行  
p 粘贴剪切板  
  
**undo/redo**  
u undo  
. redo  
  
**打开/保存/退出/改变文件**  
:e 打开文件  
:bn :bp 切换文件  
:saveas 另存为  
## 第三级 更好，更强，更快  
n<cmd> 重复某个命令n次  
<start pos><cmd><end pos> 很多命令都可以这样干  
0y$  
ye  
y2/foo  
这些命令也会被拷贝  
d v gU gu  
## 第四级 Vim超能力  
**区域选择**  
<action>a<object> 或 <action>i<object>  
action可以是任何的命令：d/y/v  
object: w/W/s/p/"/'/)/]/}  
(map (+) ("foo")) 假设光标在第一个`o`处  
```
vi" foo
va" "foo"
vi) "foo"
va) ("foo")
v2i) map (+) ("foo")
v2a) (map (+) ("foo"))  
``` 
  
**块操作**  
v/V/Ctrl+v  
典型操作：0 ctrl+v G I-- [ESC]  
I-- [ESC] I:插入 [ESC]为每一行生效  
  
**分屏**  
:split :vsplit  
ctrl+w+hjkl  

