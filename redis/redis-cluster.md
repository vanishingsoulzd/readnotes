<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Redis集群方案：Codis & Redis Cluster](#redis%E9%9B%86%E7%BE%A4%E6%96%B9%E6%A1%88codis--redis-cluster)
  - [Codis](#codis)
    - [整体架构](#%E6%95%B4%E4%BD%93%E6%9E%B6%E6%9E%84)
    - [关键技术原理](#%E5%85%B3%E9%94%AE%E6%8A%80%E6%9C%AF%E5%8E%9F%E7%90%86)
      - [数据在集群中的分布](#%E6%95%B0%E6%8D%AE%E5%9C%A8%E9%9B%86%E7%BE%A4%E4%B8%AD%E7%9A%84%E5%88%86%E5%B8%83)
      - [集群扩容和数据迁移](#%E9%9B%86%E7%BE%A4%E6%89%A9%E5%AE%B9%E5%92%8C%E6%95%B0%E6%8D%AE%E8%BF%81%E7%A7%BB)
      - [对单实例客户端的兼容性](#%E5%AF%B9%E5%8D%95%E5%AE%9E%E4%BE%8B%E5%AE%A2%E6%88%B7%E7%AB%AF%E7%9A%84%E5%85%BC%E5%AE%B9%E6%80%A7)
      - [集群可靠性](#%E9%9B%86%E7%BE%A4%E5%8F%AF%E9%9D%A0%E6%80%A7)
  - [Codis 和 Redis Cluster 的对比](#codis-%E5%92%8C-redis-cluster-%E7%9A%84%E5%AF%B9%E6%AF%94)
  - [参考资料](#%E5%8F%82%E8%80%83%E8%B5%84%E6%96%99)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Redis集群方案：Codis & Redis Cluster

## Codis

### 整体架构
Codis 集群中包含了 4 类关键组件。
- codis server：这是进行了二次开发的 Redis 实例，其中增加了额外的数据结构，支持数据迁移操作，主要负责处理具体的数据读写请求。
- codis proxy：负责将客户端请求转发给 codis server。
- Zookeeper 集群：保存集群元数据，例如数据位置信息和 codis proxy 信息。
- codis dashboard 和 codis fe：共同组成了集群管理工具。其中，codis dashboard 负责执行集群管理工作，包括增删 codis server、codis proxy 和进行数据迁移。而 codis fe 负责提供 dashboard 的 Web 操作界面，便于我们直接在 Web 界面上进行集群管理。  
![Codis整体架构](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/redis/pics/redis-cluster/codis.png)  

### 关键技术原理
#### 数据在集群中的分布
在 Codis 集群中，一个数据应该保存在哪个 codis server 上，这是通过逻辑槽（Slot）映射来完成的，具体来说，总共分成两步。  
- Codis 集群一共有 1024 个 Slot，编号依次是 0 到 1023。我们可以把这些 Slot 手动分配给 codis server，也可以让 codis dashboard 进行自动分配。
- 当客户端要读写数据时，会使用 CRC32 算法计算数据 key 的哈希值，并把这个哈希值对 1024 取模。而取模后的值，则对应 Slot 的编号。此时，根据第一步分配的 Slot 和 server 对应关系，我们就可以知道数据保存在哪个 server 上了。  

Slot 和 codis server 的映射关系称为数据路由表（简称路由表）。  
在 codis dashboard 上分配好路由表后，dashboard 会把路由表发送给 codis proxy，dashboard 也会把路由表保存在 Zookeeper 中。  
codis-proxy 会把路由表缓存在本地，当它接收到客户端请求后，直接查询本地的路由表，就可以完成正确的请求转发了。  
![Codis Route Map](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/redis/pics/redis-cluster/codis_route_map.png)  

#### 集群扩容和数据迁移
Codis 集群扩容包括了两方面：增加 codis server 和增加 codis proxy。  

**codis server扩容**  
增加 codis server的过程：
- 启动新的 codis server，将它加入集群
- 把部分数据迁移到新的 server  

数据迁移流程：
- 在源 server 上，Codis 从要迁移的 Slot 中随机选择一个数据，发送给目的 server。
- 目的 server 确认收到数据后，会给源 server 返回确认消息。这时，源 server 会在本地将刚才迁移的数据删除。
- Codis 会不断重复这个迁移过程，直到要迁移的 Slot 中的数据全部迁移完成。  

Codis实现了两种迁移模式：同步迁移和异步迁移。  
同步迁移是指，在数据从源 server 发送给目的 server 的过程中，源 server 是阻塞的，无法处理新的请求操作。如果迁移的数据是一个 bigkey，源 server 就会阻塞较长时间，无法及时处理用户请求。  
而异步迁移避免了数据迁移时对源server的阻塞。对于bigkey，采用了拆分指令的方式。  

**codis proxy扩容**  
启动 proxy，再通过 codis dashboard 把 proxy 加入集群。  
此时，codis proxy 的访问连接信息都会保存在 Zookeeper 上。所以，当新增了 proxy 后，Zookeeper 上会有最新的访问列表，客户端就可以从 Zookeeper 上读取 proxy 访问列表，把请求发送给新增的 proxy。  
这样一来，客户端的访问压力就可以在多个 proxy 上分担处理了。  
![codis proxy扩容](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/redis/pics/redis-cluster/add_codis_proxy.png)  

#### 对单实例客户端的兼容性
在使用Redis Cluster的时候，集群模式的某些命令在单实例上是没有的。所以，客户端需要增加和集群功能相关的命令操作的支持。这对于业务应用的兼容性来说，并不是特别友好。  

Codis 集群在设计时，就充分考虑了对现有单实例客户端的兼容性。  
Codis 使用 codis  proxy 直接和客户端连接，codis proxy 是和单实例客户端兼容的。而和集群相关的管理工作（例如请求转发、数据迁移等），都由 codis proxy、codis dashboard 这些组件来完成，不需要客户端参与。  
这样一来，业务应用使用 Codis 集群时，就不用修改客户端了，可以复用和单实例连接的客户端，既能利用集群读写大容量数据，又避免了修改客户端增加复杂的操作逻辑，保证了业务代码的稳定性和兼容性。  

#### 集群可靠性
codis server 其实就是 Redis 实例，只不过增加了和集群操作相关的命令。Redis 的主从复制机制和哨兵机制在 codis server 上都是可以使用的，所以，Codis 就使用主从集群来保证 codis server 的可靠性。简单来说就是，Codis 给每个 server 配置从库，并使用哨兵机制进行监控，当发生故障时，主从库可以进行切换，从而保证了 server 的可靠性。  
在这种配置情况下，每个 server 就成为了一个 server group，每个 group 中是一主多从的 server。数据分布使用的 Slot，也是按照 group 的粒度进行分配的。同时，codis proxy 在转发请求时，也是按照数据所在的 Slot 和 group 的对应关系，把写请求发到相应 group 的主库，读请求发到 group 中的主库或从库上。  
![codis reliability](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/redis/pics/redis-cluster/codis_reliability.png)  

## Codis 和 Redis Cluster 的对比

|对比维度|Codis|Redis Cluster|
|:----:|:----:|:----:|
|数据路由信息|保存在ZK，proxy在本地缓存|每个实例都保存一份|
|集群扩容|增加codis server和codis proxy|增加Redis实例|
|数据迁移|支持同步/异步迁移|支持同步迁移|
|客户端兼容性|兼容单实例客户端|需要开发支持Cluster功能的客户端|
|可靠性|codis server/codis proxy/ZK|使用主从集群保证可靠性|

## 参考资料
[Redis核心技术与实战](https://time.geekbang.org/column/intro/100056701)  
[Redis集群化方案对比：Codis、Twemproxy、Redis Cluster](http://kaito-kidd.com/2020/07/07/redis-cluster-codis-twemproxy/)  
