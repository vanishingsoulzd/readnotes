<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Redis](#redis)
  - [简介](#%E7%AE%80%E4%BB%8B)
    - [特点](#%E7%89%B9%E7%82%B9)
    - [Redis 与其他 key-value 存储有什么不同](#redis-%E4%B8%8E%E5%85%B6%E4%BB%96-key-value-%E5%AD%98%E5%82%A8%E6%9C%89%E4%BB%80%E4%B9%88%E4%B8%8D%E5%90%8C)
    - [架构](#%E6%9E%B6%E6%9E%84)
    - [使用场景及原因](#%E4%BD%BF%E7%94%A8%E5%9C%BA%E6%99%AF%E5%8F%8A%E5%8E%9F%E5%9B%A0)
  - [配置](#%E9%85%8D%E7%BD%AE)
  - [Redis 支持的数据类型](#redis-%E6%94%AF%E6%8C%81%E7%9A%84%E6%95%B0%E6%8D%AE%E7%B1%BB%E5%9E%8B)
    - [跳表](#%E8%B7%B3%E8%A1%A8)
  - [命令](#%E5%91%BD%E4%BB%A4)
  - [特性](#%E7%89%B9%E6%80%A7)
    - [持久化](#%E6%8C%81%E4%B9%85%E5%8C%96)
  - [复制](#%E5%A4%8D%E5%88%B6)
    - [同步](#%E5%90%8C%E6%AD%A5)
    - [命令传播](#%E5%91%BD%E4%BB%A4%E4%BC%A0%E6%92%AD)
    - [SYNC命令的缺陷](#sync%E5%91%BD%E4%BB%A4%E7%9A%84%E7%BC%BA%E9%99%B7)
    - [2.8版本开始使用的新版复制](#28%E7%89%88%E6%9C%AC%E5%BC%80%E5%A7%8B%E4%BD%BF%E7%94%A8%E7%9A%84%E6%96%B0%E7%89%88%E5%A4%8D%E5%88%B6)
    - [部分重同步的实现](#%E9%83%A8%E5%88%86%E9%87%8D%E5%90%8C%E6%AD%A5%E7%9A%84%E5%AE%9E%E7%8E%B0)
    - [复制的步骤](#%E5%A4%8D%E5%88%B6%E7%9A%84%E6%AD%A5%E9%AA%A4)
    - [Sentinel](#sentinel)
    - [集群](#%E9%9B%86%E7%BE%A4)
    - [安全](#%E5%AE%89%E5%85%A8)
    - [基准测试](#%E5%9F%BA%E5%87%86%E6%B5%8B%E8%AF%95)
    - [客户端连接](#%E5%AE%A2%E6%88%B7%E7%AB%AF%E8%BF%9E%E6%8E%A5)
    - [分区](#%E5%88%86%E5%8C%BA)
  - [面试题](#%E9%9D%A2%E8%AF%95%E9%A2%98)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Redis

## 简介
Redis 是开源的，遵守 BSD 协议，使用 C 语言开发。可以用来作为数据库、缓存和消息队列。

### 特点
- 高性能：Redis 将所有数据集存储在内存中，读写性能很高。Redis 支持 Pipelining 命令，可一次发送多条命令来提高吞吐率，减少通信延迟。
- 持久化：当所有数据都存在于内存中时，可以根据自上次保存以来经过的时间和/或更新次数，使用灵活的策略将更改异步保存在磁盘上。Redis 支持仅附加文件（AOF）持久化模式。
- 数据结构：Redis 支持各种类型的数据结构，例如字符串、散列、集合、列表、带有范围查询的有序集、位图、超级日志和带有半径查询的地理空间索引。
- 原子操作：处理不同数据类型的 Redis 操作是原子操作，因此可以安全地 SET 或 INCR 键，添加和删除集合中的元素等。
- 支持的语言：Redis支持许多语言。
- 主从复制：Redis 遵循非常简单快速的主/从复制。
- 分片：Redis 支持分片。
- 可移植：Redis 是用 C 编写的，适用于大多数 POSIX 系统，如 Linux、BSD、Mac OS X、Solaris 等。

### Redis 与其他 key-value 存储有什么不同
- Redis 支持数据的持久化，可以将内存中的数据保存在磁盘中，重启的时候可以再次加载到内存使用。
- Redis 不仅支持简单的 key-value 类型的数据，同时还提供 list，set，zset，hash 等数据结构的存储。
- Redis 支持主从复制，即 master-slave 模式的数据备份。
- Redis 有着更为复杂的数据结构并且提供对它们的原子性操作，这是一个不同于其他数据库的进化路径。Redis 的数据类型都是基于基本数据结构的同时对程序员透明，无需进行额外的抽象。
- Redis 运行在内存中但是可以持久化到磁盘，所以在对不同数据集进行高速读写时需要权衡内存，因为数据量不能大于硬件内存。在内存数据库方面的另一个优点是，相比在磁盘上相同的复杂的数据结构，在内存中操作起来非常简单，这样 Redis 可以做很多内部复杂性很强的事情。同时，因 RDB 和 AOF 两种磁盘持久化方式是不适合随机访问，因为它们是顺序写入的。

### 架构
Redis 主要由有两个程序组成：
- Redis 客户端 redis-cli
- Redis 服务器 redis-server

### 使用场景及原因
[Redis 使用场景](https://www.redis.com.cn/topics/why-use-redis.html)  

## 配置
Redis 的配置文件位于Redis 安装目录(`/usr/local/etc`)下，文件名为redis.conf。可以通过`CONFIG [get|set]`命令查看或设置配置项。  
[常用配置参数说明](https://www.redis.com.cn/redis-configuration.html)  
连接远程 Redis，`redis-cli -h host -p port -a password`  

## Redis 支持的数据类型
- 字符串：`set/get`，在 Redis 数据库中，字符串是二进制安全的。这意味着它们具有已知长度，并且不受任何特殊终止字符的影响。可以在一个字符串中存储最多 512 兆字节的内容。
- 哈希：`hmset/hgetall`，哈希是键值对的集合。在 Redis 中，哈希是字符串字段和字符串值之间的映射。因此，它们适合表示对象。每个哈希可以存储多达 2^32 – 1 个键值对。
- 列表：`lpush/rpush/lrange`，Redis 列表定义为字符串列表，按插入顺序排序。可以将元素添加到 Redis 列表的头部或尾部。列表的最大长度为 2^32 – 1 个元素（超过 40 亿个元素）。
- 集合：`sadd/smembers`，集合（set）是 Redis 数据库中的无序字符串集合。在 Redis 中，添加，删除和查找的时间复杂度是 O(1)。集合中的最大成员数为 2^32 - 1 个元素（超过 40 亿个元素）。
- 有序集合：`zadd/zrangbyscore`，排序集的每个成员都与一个分数相关联，该分数用于获取从最小到最高分数的有序排序集。虽然成员是独特的，但可以重复分数。
- 位图
- 基数统计
删除键：`del`  

### 跳表

## 命令
[Redis 命令](https://www.redis.net.cn/order/)  
发布订阅：`subscribe/publish`  
事务：`multi/exec/discard/watch`  
脚本：`eval`  
备份和恢复：`save/bgsave`，`config get dir`  

## 特性
### 持久化
redis 提供了两种持久化的方式，分别是RDB（Redis DataBase）和AOF（Append Only File）。  
RDB，简而言之，就是在不同的时间点，将 redis 存储的数据生成快照并存储到磁盘等介质上；

AOF，则是换了一个角度来实现持久化，那就是将 redis 执行过的所有写指令记录下来，在下次 redis 重新启动时，只要把这些写指令从前到后再重复执行一遍，就可以实现数据恢复了。

其实 RDB 和 AOF 两种方式也可以同时使用，在这种情况下，如果 redis 重启的话，则会优先采用 AOF 方式来进行数据恢复，这是因为 AOF 方式的数据恢复完整度更高。

如果你没有数据持久化的需求，也完全可以关闭 RDB 和 AOF 方式，这样的话，redis 将变成一个纯内存数据库，就像 memcache 一样。

## 复制
`slaveof <master_ip> <master_port>`
Redis的复制功能分为同步（sync）和命令传播（command propagate）两个操作:
- 同步操作用于将从服务器的数据库状态更新至主服务器当前所处的数据库状态。
- 命令传播操作则用于在主服务器的数据库状态被修改，导致主从服务器的数据库状态出现不一致时，让主从服务器的数据库重新回到一致状态。

### 同步  
SYNC步骤：
- 从服务器向主服务器发送SYNC命令。
- 收到SYNC命令的主服务器执行BGSAVE命令，在后台生成一个RDB文件，并使用一个缓冲区记录从现在开始执行的所有写命令。
- 当主服务器的BGSAVE命令执行完毕时，主服务器会将BGSAVE命令生成的RDB文件发送给从服务器，从服务器接收并载入这个RDB文件，将自己的数据库状态更新至主服务器执行BGSAVE命令时的数据库状态。
- 主服务器将记录在缓冲区里面的所有写命令发送给从服务器，从服务器执行这些写命令，将自己的数据库状态更新至主服务器数据库当前所处的状态。  
![SYNC命令过程](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/redis/pics/sync.png)  

### 命令传播  
主服务器会将自己执行的写命令，发送给从服务器执行，当从服务器执行了相同的写命令之后，主从服务器将再次回到一致状态。  

### SYNC命令的缺陷
每次从服务器断线后重连，都要让主从服务器重新执行一次SYNC命令，非常低效。
- 主服务器需要执行BGSAVE命令来生成RDB文件，这个生成操作会耗费主服务器大量的CPU、内存和磁盘I/O资源。
- 主服务器需要将自己生成的RDB文件发送给从服务器，这个发送操作会耗费主从服务器大量的网络资源（带宽和流量），并对主服务器响应命令请求的时间产生影响。
- 接收到RDB文件的从服务器需要载入主服务器发来的RDB文件，并且在载入期间，从服务器会因为阻塞而没办法处理命令请求。

### 2.8版本开始使用的新版复制  
为了解决旧版复制功能在处理断线重复制情况时的低效问题，Redis从2.8版本开始，使用PSYNC命令代替SYNC命令来执行复制时的同步操作。  
PSYNC命令具有完整重同步（full resynchronization）和部分重同步（partial resynchronization）两种模式。  
![SYNC命令过程](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/redis/pics/partial_sync.png)  

### 部分重同步的实现  
- 主从服务器的复制偏移量
- 主服务器的复制积压缓冲区
- 服务器的运行ID(run ID)，用于判断重连的主服务器是不是断线前的主服务器  
![repl-backlog](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/redis/pics/repl-backlog.png)  

### 复制的步骤
- 设置主服务器的地址和端口
- 建立套接字连接
- 发送PING命令
- 身份验证
- 发送端口信息
- 同步
- 命令传播
- 心跳检测
  - 检测主从服务器的网络连接状态
  - 辅助实现min-slaves配置选项
  - 检测命令丢失

### Sentinel
**通信**  
`INFO`:每十秒一次(master/slave/sentinel)，故障转移时每秒一次(观察role)
`PING`:每秒一次(master/slave/sentinel)  
`__sentinel__:hello`: 每两秒一次(master/slave)(用于发现其他sentinel)  

**故障转移操作**  
- 首先，Sentinel系统会挑选server1属下的其中一个从服务器，并将这个被选中的从服务器升级为新的主服务器。
- 之后，Sentinel系统会向server1属下的所有从服务器发送新的复制指令，让它们成为新的主服务器的从服务器，当所有从服务器都开始复制新的主服务器时，故障转移操作执行完毕。
- 另外，Sentinel还会继续监视已下线的server1，并在它重新上线时，将它设置为新的主服务器的从服务器。 

**启动**  
`redis-sentinel /usr/local/etc/redis-sentinel.conf`  
`redis-server /usr/local/etc/redis-sentinel.conf --sentinel`  
执行步骤：  
- 初始化服务器。
- 将普通Redis服务器使用的代码替换成Sentinel专用代码。
- 初始化Sentinel状态。
- 根据给定的配置文件，初始化Sentinel的监视主服务器列表。
- 创建连向主服务器的网络连接。
```
struct sentinelState {

    // 当前纪元，用于实现故障转移
    uint64_t current_epoch;

    // 保存了所有被这个sentinel监视的主服务器
    // 字典的键是主服务器的名字
    // 字典的值则是一个指向sentinelRedisInstance结构的指针
    dict *masters;

    // 是否进入了TILT模式？
    int tilt;

    // 目前正在执行的脚本的数量
    int running_scripts;

    // 进入TILT模式的时间
    mstime_t tilt_start_time;

    // 最后一次执行时间处理器的时间
    mstime_t previous_time;

    // 一个FIFO队列，包含了所有需要执行的用户脚本
    list *scripts_queue;

} sentinel;
```
```
typedef struct sentinelRedisInstance {

    // 标识值，记录了实例的类型，以及该实例的当前状态
    // 主观下线: SRI_S_DOWN
    // 客观下线: SRI-O_DOWN
    // SRI_MASTER/SRI_SLAVE/SRI_SENTINEL
    int flags;

    // 实例的名字
    // 主服务器的名字由用户在配置文件中设置
    // 从服务器以及Sentinel的名字由Sentinel自动设置
    // 格式为ip:port，例如"127.0.0.1:26379"
    char *name;

    // 实例的运行ID
    char *runid;

    // 配置纪元，用于实现故障转移
    uint64_t config_epoch;

    // 实例的地址
    sentinelAddr *addr;

    // SENTINEL down-after-milliseconds选项设定的值
    // 实例无响应多少毫秒之后才会被判断为主观下线（subjectively down）
    mstime_t down_after_period;

    // SENTINEL monitor <master-name> <IP> <port> <quorum>选项中的quorum参数
    // 判断这个实例为客观下线（objectively down）所需的支持投票数量
    int quorum;

    // SENTINEL parallel-syncs <master-name> <number>选项的值
    // 在执行故障转移操作时，可以同时对新的主服务器进行同步的从服务器数量
    int parallel_syncs;

    // SENTINEL failover-timeout <master-name> <ms>选项的值
    // 刷新故障迁移状态的最大时限
    mstime_t failover_timeout;

    // 记录了主服务器属下的从服务器名单
    dict *slaves
    
    // 所有同样监视这个主服务器的其他Sentinel
    dict *sentinels

    // 对于slave, INFO 命令回复以下内容
    slave_master-link-status
    slave_priority
    slave_repl_offset
    // ...

} sentinelRedisInstance;
```

**检测主观下线状态**  
每秒一次PING  
有效回复：+PONG/-LOADING/-MASTERDOWN  
在`down-after-milliseconds`配置的时间内，连续返回无效回复，认为主观下线  

**检测客观下线状态**  
- 发送SENTINEL is-master-down-by-addr命令，向其他sentinel确认master是否下线
`SENTINEL is-master-down-by-addr <ip> <port> <current_epoch> <runid>`  
- 接收SENTINEL is-master-down-by-addr命令的sentinel回复自己的检查结果
- 接收SENTINEL is-master-down-by-addr 命令的回复，如果同意master下线的数量(包括自己)大于`quorum`配置，则认为客观下线  

**选举领头Sentinel**  
- 所有在线的Sentinel都有被选为领头Sentinel的资格，换句话说，监视同一个主服务器的多个在线Sentinel中的任意一个都有可能成为领头Sentinel。
- 每次进行领头Sentinel选举之后，不论选举是否成功，所有Sentinel的配置纪元（con guration epoch）的值都会自增一次。配置纪元实际上就是一个计数器，并没有什么特别的。
- 在一个配置纪元里面，所有Sentinel都有一次将某个Sentinel设置为局部领头Sentinel的机会，并且局部领头一旦设置，在这个配置纪元里面就不能再更改。
- 每个发现主服务器进入客观下线的Sentinel都会要求其他Sentinel将自己设置为局部领头Sentinel。
- 当一个Sentinel（源Sentinel）向另一个Sentinel（目标Sentinel）发送SENTINEL is-master-down-by-addr命令，并且命令中的runid参数不是*符号而是源Sentinel的运行ID时，这表示源Sentinel要求目标Sentinel将前者设置为后者的局部领头Sentinel。
- Sentinel设置局部领头Sentinel的规则是先到先得：最先向目标Sentinel发送设置要求的源Sentinel将成为目标Sentinel的局部领头Sentinel，而之后接收到的所有设置要求都会被目标Sentinel拒绝。
- 目标Sentinel在接收到SENTINEL is-master-down-by-addr命令之后，将向源Sentinel返回一条命令回复，回复中的leader_runid参数和leader_epoch参数分别记录了目标Sentinel的局部领头Sentinel的运行ID和配置纪元。
- 源Sentinel在接收到目标Sentinel返回的命令回复之后，会检查回复中leader_epoch参数的值和自己的配置纪元是否相同，如果相同的话，那么源Sentinel继续取出回复中的leader_runid参数，如果leader_runid参数的值和源Sentinel的运行ID一致，那么表示目标Sentinel将源Sentinel设置成了局部领头Sentinel。
- 如果有某个Sentinel被半数以上的Sentinel设置成了局部领头Sentinel，那么这个Sentinel成为领头Sentinel。举个例子，在一个由10个Sentinel组成的Sentinel系统里面，只要有大于等于10/2+1=6个Sentinel将某个Sentinel设置为局部领头Sentinel，那么被设置的那个Sentinel就会成为领头Sentinel。
- 因为领头Sentinel的产生需要半数以上Sentinel的支持，并且每个Sentinel在每个配置纪元里面只能设置一次局部领头Sentinel，所以在一个配置纪元里面，只会出现一个领头Sentinel。
- 如果在给定时限内，没有一个Sentinel被选举为领头Sentinel，那么各个Sentinel将在一段时间之后再次进行选举，直到选出领头Sentinel为止。

**故障转移**  
- 在已下线主服务器属下的所有从服务器里面，挑选出一个从服务器，并将其转换为主服务器。
- 让已下线主服务器属下的所有从服务器改为复制新的主服务器。
- 将已下线主服务器设置为新的主服务器的从服务器，当这个旧的主服务器重新上线时，它就会成为新的主服务器的从服务器。
`SLAVEOF on one`  
`SLAVEOF <new-master-ip> <new-master-port>`  

**选出新的主服务器**  
领头Sentinel会将已下线主服务器的所有从服务器保存到一个列表里面，然后按照以下规则，一项一项地对列表进行过滤：
- 删除列表中所有处于下线或者断线状态的从服务器，这可以保证列表中剩余的从服务器都是正常在线的。
- 删除列表中所有最近五秒内没有回复过领头Sentinel的INFO命令的从服务器，这可以保证列表中剩余的从服务器都是最近成功进行过通信的。
- 删除所有与已下线主服务器连接断开超过down-after-milliseconds * 10毫秒的从服务器：down-after-milliseconds选项指定了判断主服务器下线所需的时间，而删除断开时长超过down-after-milliseconds * 10毫秒的从服务器，则可以保证列表中剩余的从服务器都没有过早地与主服务器断开连接，换句话说，列表中剩余的从服务器保存的数据都是比较新的。
之后，领头Sentinel将根据从服务器的优先级，对列表中剩余的从服务器进行排序，并选出其中优先级最高的从服务器。
如果有多个具有相同最高优先级的从服务器，那么领头Sentinel将按照从服务器的复制偏移量，对具有相同最高优先级的所有从服务器进行排序，并选出其中偏移量最大的从服务器（复制偏移量最大的从服务器就是保存着最新数据的从服务器）。
最后，如果有多个优先级最高、复制偏移量最大的从服务器，那么领头Sentinel将按照运行ID对这些从服务器进行排序，并选出其中运行ID最小的从服务器。   
在发送`SLAVEOF no one`命令之后，领头Sentinel会以每秒一次的频率（平时是每十秒一次），向被升级的从服务器发送INFO命令，并观察命令回复中的角色（role）信息，当被升级服务器的role从原来的slave变为master时，领头Sentinel就知道被选中的从服务器已经顺利升级为主服务器了。  

### 集群
**集群基本配置**  
```
cluster-enabled yes
port 7000
cluster-config-file nodes.conf
cluster-node-timeout 5000(毫秒)
appendonly yes
```

**集群数据结构**   
`clusterNode/clusterState/clusterLink`  
![clusterState](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/redis/pics/clusterState.png)  
```
struct clusterNode {

  // 创建节点的时间
  mstime_t ctime;

  // 节点的名字，由40个十六进制字符组成
  // 例如68eef66df23420a5862208ef5b1a7005b806f2ff
  char name[REDIS_CLUSTER_NAMELEN];

  // 节点标识
  // 使用各种不同的标识值记录节点的角色（比如主节点或者从节点），
  // 以及节点目前所处的状态（比如在线或者下线）。
  // REDIS_NODE_MASTER/REDIS_NODE_SLAVE/REDIS_NODE_PFAIL
  int flags;

  // 节点当前的配置纪元，用于实现故障转移
  uint64_t configEpoch;

  // 节点的IP地址
  char ip[REDIS_IP_STR_LEN];

  // 节点的端口号
  int port;

  // 保存连接节点所需的有关信息
  clusterLink *link;

  // 槽指派信息
  unsigned char slots[16384/8];
  int numslots;
  
  // 主从
  // 如果这是一个从节点，那么指向主节点
  struct clusterNode *slaveof;
  // 正在复制这个主节点的从节点数量
  int numslaves;
  // 一个数组
  // 每个数组项指向一个正在复制这个主节点的从节点的clusterNode结构
  struct clusterNode **slaves;

  // 故障检测
  // 一个链表，记录了所有其他节点对该节点的下线报告
  list *fail_reports;

  // ...

};

typedef struct clusterLink {

  // 连接的创建时间
  mstime_t ctime;

  // TCP 套接字描述符
  int fd;

  // 输出缓冲区，保存着等待发送给其他节点的消息（message）。
  sds sndbuf;

  // 输入缓冲区，保存着从其他节点接收到的消息。
  sds rcvbuf;

  // 与这个连接相关联的节点，如果没有的话就为NULL 
  struct clusterNode *node;

} clusterLink;

typedef struct clusterState {

  // 指向当前节点的指针
  clusterNode *myself;

  // 集群当前的配置纪元，用于实现故障转移
  uint64_t currentEpoch;

  // 集群当前的状态：是在线还是下线
  int state;

  // 集群中至少处理着一个槽的节点的数量
  int size;

  // 集群节点名单（包括myself节点）
  // 字典的键为节点的名字，字典的值为节点对应的clusterNode结构
  dict *nodes;
  
  // 槽指派信息
  clusterNode *slots[16384];
  
  // 保存槽和键之间的关系
  zskiplist *slots_to_keys;

  // 重新分片
  // 当前节点正在从其他节点导入的槽
  clusterNode *importing_slots_from[16384];
  // 当前节点正在迁移至其他节点的槽
  clusterNode *migrating_slots_to[16384];
  // ...

} clusterState;
```

**连接各个节点**  
方式1：`cluster meet <ip> <port>`  
- 节点A会为节点B创建一个clusterNode结构，并将该结构添加到自己的clusterState.nodes字典里面。
- 节点A向节点B发送一条MEET消息（message）。
- 节点B将接收到节点A发送的MEET消息，节点B会为节点A创建一个clusterNode结构，并将该结构添加到自己的clusterState.nodes字典里面。
- 节点B将向节点A返回一条PONG消息。
- 节点A将接收到节点B返回的PONG消息，就知道节点B已经成功地接收到了自己发送的MEET消息。
- 节点A将向节点B返回一条PING消息。
- 节点B将接收到节点A返回的PING消息，就可以知道节点A已经成功地接收到了自己返回的PONG消息，握手完成。
![cluster meet](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/redis/pics/cluster-meet.png)  

方式2：`redis-cli --cluster create <ip> <port> --cluster-replicas 1`流程：  
- 尝试在各个节点上分配哈希槽
- 尝试对slave进行反亲和性优化(todo)
- 是否应用以上配置
- 创建节点配置文件(nodes.conf)
- 为各个节点分配不同的配置纪元(用于实现故障转移)
- 发送`cluster meet`加入集群
- 执行集群检查(所有节点都同意插槽配置/所有槽都被分配)
`--cluster-replicas 1`表示为每一个主服务器配一个从服务器。  

**槽指派**  
`cluster addslots 1 2`  
`redis-cli -c -h 127.0.0.1 -p 7000 cluster addslots $(seq 0 16383)`
![slots array](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/redis/pics/slots-array.png)  
![clusterState.slots](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/redis/pics/clusterState-slots-array.png)  

**计算key属于哪个slot**   
`cluster keyslot`  
```
def slot_number(key):
    return CRC16(key) & 16383
```
其中CRC16(key)语句用于计算键key的CRC-16校验和，而& 16383语句则用于计算出一个介于0至16383之间的整数作为键key的槽号。  

**节点数据库的实现**  
使用clusterState结构中的slots_to_keys跳跃表来保存槽和键之间的关系。
- 每当节点往数据库中添加一个新的键值对时，节点就会将这个键以及键的槽号关联到slots_to_keys跳跃表。
- 当节点删除数据库中的某个键值对时，节点就会在slots_to_keys跳跃表解除被删除键与槽号的关联。
通过在slots_to_keys跳跃表中记录各个数据库键所属的槽，节点可以很方便地对属于某个或某些槽的所有数据库键进行批量操作，例如命令CLUSTER GETKEYSINSLOT <slot> <count>命令可以返回最多count个属于槽slot的数据库键，而这个命令就是通过遍历slots_to_keys跳跃表来实现的。  

**重新分片的实现**  
Redis集群的重新分片操作是由Redis的集群管理软件redis-trib负责执行的，Redis提供了进行重新分片所需的所有命令，而redis-trib则通过向源节点和目标节点发送命令来进行重新分片操作。  
- redis-trib对目标节点发送CLUSTER SETSLOT <slot> IMPORTING <source_id> 命令，让目标节点准备好从源节点导入（import）属于槽slot的键值对。
- redis-trib对源节点发送CLUSTER SETSLOT <slot> MIGRATING <target_id>命令，让源节点准备好将属于槽slot的键值对迁移（migrate）至目标节点。
- redis-trib向源节点发送CLUSTER GETKEYSINSLOT <slot> <count>命令，获得最多count个属于槽slot的键值对的键名（key name）。
- 对于步骤3获得的每个键名，redis-trib都向源节点发送一个MIGRATE <target_ip> <target_port> <key_name> 0 <timeout>命令，将被选中的键原子地从源节点迁移至目标节点。
- 重复执行步骤3和步骤4，直到源节点保存的所有属于槽slot的键值对都被迁移至目标节点为止。
- redis-trib向集群中的任意一个节点发送CLUSTER SETSLOT <slot> NODE <target_id>命令，将槽slot指派给目标节点，这一指派信息会通过消息发送至整个集群，最终集群中的所有节点都会知道槽slot已经指派给了目标节点。
![slot resharding](https://gitlab.com/vanishingsoulzd/readnotes/-/blob/master/redis/pics/slot-resharding.png)  

**设置从节点**  
`cluster replicate <master_node_id>`  
- 设置`clusterState.myself.slaveof/clusterState.myself.flags`
- 复制：`slaveof <master_ip> <master_port>`  

**故障检测**  
如果在一个集群里面，半数以上负责处理槽的主节点都将某个主节点x报告为疑似下线，那么这个主节点x将被标记为已下线（FAIL），将主节点x标记为已下线的节点会向集群广播一条关于主节点x的FAIL消息，所有收到这条FAIL消息的节点都会立即将主节点x标记为已下线。  

**故障转移**  
- 复制下线主节点的所有从节点里面，会有一个从节点被选中。
- 被选中的从节点会执行SLAVEOF no one命令，成为新的主节点。
- 新的主节点会撤销所有对已下线主节点的槽指派，并将这些槽全部指派给自己。
- 新的主节点向集群广播一条PONG消息，这条PONG消息可以让集群中的其他节点立即知道这个节点已经由从节点变成了主节点，并且这个主节点已经接管了原本由已下线节点负责处理的槽。
- 新的主节点开始接收和自己负责处理的槽有关的命令请求，故障转移完成。

**选举新的主节点(参考选举领头Sentinel)**  
1）集群的配置纪元是一个自增计数器，它的初始值为0。
2）当集群里的某个节点开始一次故障转移操作时，集群配置纪元的值会被增一。
3）对于每个配置纪元，集群里每个负责处理槽的主节点都有一次投票的机会，而第一个向主节点要求投票的从节点将获得主节点的投票。
4）当从节点发现自己正在复制的主节点进入已下线状态时，从节点会向集群广播一条CLUSTERMSG_TYPE_FAILOVER_AUTH_REQUEST消息，要求所有收到这条消息、并且具有投票权的主节点向这个从节点投票。
5）如果一个主节点具有投票权（它正在负责处理槽），并且这个主节点尚未投票给其他从节点，那么主节点将向要求投票的从节点返回一条CLUSTERMSG_TYPE_FAILOVER_AUTH_ACK消息，表示这个主节点支持从节点成为新的主节点。
6）每个参与选举的从节点都会接收CLUSTERMSG_TYPE_FAILOVER_AUTH_ACK消息，并根据自己收到了多少条这种消息来统计自己获得了多少主节点的支持。
7）如果集群里有N个具有投票权的主节点，那么当一个从节点收集到大于等于N/2+1张支持票时，这个从节点就会当选为新的主节点。
8）因为在每一个配置纪元里面，每个具有投票权的主节点只能投一次票，所以如果有N个主节点进行投票，那么具有大于等于N/2+1张支持票的从节点只会有一个，这确保了新的主节点只会有一个。
9）如果在一个配置纪元里面没有从节点能收集到足够多的支持票，那么集群进入一个新的配置纪元，并再次进行选举，直到选出新的主节点为止。  

**消息**  

### 安全
`config get requirepass`
`auth password`

### 基准测试
[Redis 基准测试](https://www.redis.com.cn/redis-benchmarks.html)  

### 客户端连接
`config get maxclients`
`redis-server --maxclients 10000`

### 分区

## 面试题
[Redis 面试题](https://www.redis.com.cn/redis-interview-questions.html)  
